<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';

    protected $filltable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
    ];

    protected $hidden = [
        'password'
    ];

    public function historypointtest()
    {
        return $this->hasMany('App\Models\HistoryPointTestModel', 'userid');
    }

    public function aboutme()
    {
        return $this->hasOne('App\Models\AboutMeModel', 'user_id');
    }
}
