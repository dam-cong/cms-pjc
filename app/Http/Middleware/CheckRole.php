<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $levevlid=$request->route('levelid');
        switch ($levevlid) {
            case "1":
                if(auth()->user()->role == 1 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "2":
                if(auth()->user()->role == 2 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "3":
                if(auth()->user()->role == 3 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "4":
                if(auth()->user()->role == 4 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "5":
                if(auth()->user()->role == 5 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "6":
                if(auth()->user()->role == 6 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "7":
                if(auth()->user()->role == 7 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "8":
                if(auth()->user()->role == 8 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "9":
                if(auth()->user()->role == 9 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "10":
                if(auth()->user()->role == 10 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            default:
                return redirect()->route('403');
        }
    }
}
