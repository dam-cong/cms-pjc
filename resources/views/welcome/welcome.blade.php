<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">

<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="The Official Worldwide Japanese-Language Proficiency Test Website is operated by the Japan Foundation and Japan Educational Exchanges and Services." />
    <meta name="keywords" content="Japanese-Language Proficiency Test, JLPT, Japanese, test, Japan Foundation, JEES, Japanese learning, grammar, kanji, examination, grade" />
    <title>JLPT Tiếng Nhật - Đánh giá năng lực</title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta content="index,follow" name="robots" />
    <!-- for legacy browser -->
    <link rel="stylesheet" type="text/css" href="https://www.jlpt.jp/e/resource/css/legacy.css" />
    <!-- for modern browser -->
    <link rel="stylesheet" type="text/css" href="https://www.jlpt.jp/e/resource/css/modern.css" media="screen,print" />
    <!-- load styleswitcher style -->
    <link type="text/css" rel="stylesheet" href="https://www.jlpt.jp/e/resource/css/textsize-m.css" title="medium" />
    <link type="text/css" rel="alternate stylesheet" href="https://www.jlpt.jp/e/resource/css/textsize-s.css" title="small" />
    <link type="text/css" rel="alternate stylesheet" href="https://www.jlpt.jp/e/resource/css/textsize-l.css" title="large" />
    <!-- load JavaScript -->
    <script type="text/javascript" src="https://www.jlpt.jp/e/resource/script/common.js"></script>
    <script type="text/javascript" src="https://www.jlpt.jp/e/resource/script/smoothscroll.js"></script>
    <script type="text/javascript" src="https://www.jlpt.jp/e/resource/script/heightLine.js"></script>
    <script type="text/javascript" src="https://www.jlpt.jp/e/resource/script/styleswitcher.js"></script>
    <!-- load flash contents -->
    <script type="text/javascript" src="https://www.jlpt.jp/e/resource/script/swfobject.js"></script>
    <style type="text/css">
        #h1Block {
            margin-bottom: 35px;
            padding: 10px 0px 0px 0px;
            background: #fff;
            height: 251px;
        }

        #h1Block h1 img {
            margin-bottom: 0;
            float: left;
            width: 721px;
        }

        #h1Block ul {
            float: left;
            width: 210px;
            padding-right: 4px;
        }

        #leftBlock {
            display: inline;
            float: left;
            width: 565px;
            padding-right: 47px;
        }

        #leftBlock rt {
            font-size: 50%;
            text-align: justify;
            text-decoration: none;
        }

        #leftBlock #ct1 .h2Block {
            height: 46px;
            margin-bottom: 5px;
            background: url(img/index_h2_bg.gif) no-repeat left top;
        }

        #leftBlock #ct1 .h2Block h2 {
            display: inline;
            float: left;
            width: 69px;
            padding: 16px 0 0 27px;
        }

        #leftBlock #ct1 .h2Block .link {
            display: inline;
            float: right;
            width: 93px;
            padding: 16px 13px 0 0;
        }

        #leftBlock #ct1 ul li {
            border-top: 1px solid #ddd;
        }

        #leftBlock #ct1 ul li dl dt {
            display: inline;
            float: left;
            width: 150px;
            padding: 11px 0 0 8px;
            border-top: 5px solid #f4f4f4;
            color: #f60;
            font-size: 108%;
            font-weight: bold;
        }

        #leftBlock #ct1 ul li dl dt .date {
            padding-left: 19px;
            background: url(https://www.jlpt.jp/e/resource/img_common/icon_square_g.gif) no-repeat left center;
        }

        #leftBlock #ct1 ul li dl dd {
            display: inline;
            float: left;
            width: 377px;
            padding: 12px;
            font-size: 116%;
        }

        #leftBlock #ct1 ul li dl dd,
        x:-moz-broken {
            padding-top: 14px;
        }

        #leftBlock #ct1 ul li dl .noruby {
            padding-top: 22px;
        }

        #leftBlock #ct1 ul li dl .noruby,
        x:-moz-broken {
            padding-top: 15px;
        }

        #leftBlock #ct1 ul li dl dd a:link,
        #leftBlock #ct1 ul li dl dd a:visited,
        #leftBlock #ct1 ul li dl dd a:active {
            color: #666;
            text-decoration: none;
        }

        #leftBlock #ct1 ul li dl dd a:hover {
            text-decoration: underline;
        }

        #leftBlock #ct2 {
            margin-top: 10px;
        }

        #leftBlock #ct2 h2 {
            height: 46px;
            margin-bottom: 5px;
            padding: 5px 0 0 27px;
            background: url(img/index_h2_bg.gif) no-repeat left top;
        }

        #leftBlock #ct2 .lead {
            font-size: 116%;
        }

        #leftBlock #ct2 .item {
            display: inline;
            float: left;
            width: 282px;
        }

        #leftBlock #ct2 .item .pht {
            display: inline;
            float: left;
            width: 70px;
            padding-right: 14px;
            background: url(img/index_vline.gif) no-repeat right top;
        }

        #leftBlock #ct2 .item .pht img {
            padding: 1px;
            border: 1px solid #ddd;
        }

        #leftBlock #ct2 .item .text {
            display: inline;
            float: left;
            width: 163px;
            padding: 0 27px 0 8px;
        }

        #rightBlock {
            display: inline;
            float: left;
            width: 323px;
        }

        #rightBlock #sche {
            background: url(img/sche_bg.gif) no-repeat left bottom;
        }

        #rightBlock #sche #sche_in {
            padding-left: 2px;
            padding-right: 2px;
        }

        #rightBlock #sche .sche_text {
            padding: 12px 20px 20px 20px;
            text-align: left;
        }

        #rightBlock #sche .btn ul li {
            margin-bottom: 9px;
        }

        #rightBlock #sche .sche_text {
            padding: 12px 20px 20px 20px;
            text-align: left;
        }

        #rightBlock #sample {
            margin-top: 13px;
            background: url(https://www.jlpt.jp/e/img/sample_bg2.gif) no-repeat left bottom;
        }

        #rightBlock #sample dd {
            padding: 0 0 0 15px;
        }

        #rightBlock #sample .btn {
            padding: 20px 15px;
        }


        #rightBlock #sample .btn1 {
            padding: 18px 0 18px 19px;
        }

        #rightBlock #sample .btn2 {
            padding: 3px 0 8px 19px;
        }


        #rightBlock #materials {
            margin-top: 13px;
            margin-bottom: 20px;
        }
    </style>
</head>

<body>
    <div id="whole">
        <div id="skiplinkSec">
            <ul>
                <li><a href="#headAnc">Jumps to the header</a></li>
                <li><a href="#naviAnc">Jumps to the menu</a></li>
                <li><a href="#mainAnc">Jumps to the text</a></li>
            </ul>
        </div>
        <a name="pagetop" id="pagetop">Page Top</a>

        <hr />

        <div id="container">
            <div id="container-inner">
                <p><a name="headAnc" id="headAnc">Start of Header</a></p>
                <div id="headSec">
                    <div id="headSec-inner" class="clearfix">
                        <div id="logo"><a href="https://www.jlpt.jp/e/index.html"><img src="https://www.jlpt.jp/e/resource/img_common/logo.gif" width="239" height="74" alt="Japanese-Language Proficiency Test" /></a></div>
                        <div id="headRight">
                            <ul class="clearfix">
                                <li><a href="https://www.jlpt.jp/tenji.html"><img class="imgover" src="https://www.jlpt.jp/resource/img_common/head_navi_tenji.gif" width="210" height="13" alt="点字ファイルダウンロードはこちら" /></a></li>
                                <li><a href="https://www.jlpt.jp/index.html"><img class="imgover" src="https://www.jlpt.jp/resource/img_common/head_navi_lang_ja.gif" width="40" height="12" alt="日本語" /></a></li>
                                <li><a href="https://www.jlpt.jp/cn/index.html"><img class="imgover" src="https://www.jlpt.jp/resource/img_common/head_navi_lang_cn.gif" width="60" height="12" alt="简体中文" /></a></li>
                                <li><a href="https://www.jlpt.jp/tw/index.html"><img class="imgover" src="https://www.jlpt.jp/resource/img_common/head_navi_lang_tw.gif" width="60" height="12" alt="繁體中文" /></a></li>
                            </ul>
                            <div id="second" class="clearfix">
                                <div class="link"><a href="https://www.jlpt.jp/e/index.html"><img class="imgover" src="https://www.jlpt.jp/e/resource/img_common/head_navi_home.gif" width="37" height="10" alt="HOME" /></a></div>
                                <dl class="clearfix">
                                    <dt><img src="https://www.jlpt.jp/e/resource/img_common/textsize.gif" width="58" height="11" alt="FontSize" /></dt>
                                    <dd class="small"><a href="javascript:void(0)" onClick="setActiveStyleSheet('small'); return false;"><img class="imgover" src="https://www.jlpt.jp/e/resource/img_common/textsize01.gif" width="22" height="22" alt="小（しょう）" /></a></dd>
                                    <dd class="medium"><a href="javascript:void(0)" onClick="setActiveStyleSheet('medium'); return false;"><img class="imgover" src="https://www.jlpt.jp/e/resource/img_common/textsize02.gif" width="22" height="22" alt="中（ちゅう）" /></a></dd>
                                    <dd class="large"><a href="javascript:void(0)" onClick="setActiveStyleSheet('large'); return false;"><img class="imgover" src="https://www.jlpt.jp/e/resource/img_common/textsize03.gif" width="22" height="22" alt="大（だい）" /></a></dd>
                                </dl>
                                <form action="search_result.html" id="cse-search-box">
                                    <input type="hidden" name="cx" value="011774718497016608938:s3z3qdcqnze" />
                                    <input type="hidden" name="cof" value="FORID:9" />
                                    <input type="hidden" name="ie" value="UTF-8" />
                                    <input type="text" name="q" size="31" class="text" /><input type="image" name="sa" src="https://www.jlpt.jp/e/resource/img_common/search_btn.gif" />
                                </form>
                                <script type="text/javascript" src="//www.google.com/cse/brand?form=cse-search-box&lang=en"></script>
                            </div>
                        </div>
                    </div>
                </div> <!-- /head sec -->

                <hr />

                <div id="gNaviSec">
                    <div id="gNaviSec-inner" class="clearfix">
                        <a name="naviAnc" id="naviAnc">Start of Menu</a>
                        <ul class="clearfix">
                            <li><a href="about/index.html"><img class="imgover" src="../img_common/gnavi1.png" width="233" height="47" alt="What is the Japanese-Language Proficiency Test?" /></a></li>
                            <li><a href="guideline/testsections.html"><img class="imgover" src="../img_common/gnavi2.png" width="233" height="47" alt="Testing and Results Notification" /></a></li>
                            <li><a href="application/domestic_index.html"><img class="imgover" src="../img_common/gnavi3.png" width="234" height="47" alt="Registration Process" /></a></li>
                            <li><a href="samples/forlearners.html"><img class="imgover" src="../img_common/gnavi4.png" width="235" height="47" alt="Sample Questions" /></a></li>
                            <li><a href="reference/books.html"><img class="imgover" src="../img_common/gnavi5.png" width="233" height="48" alt="Books and References" /></a></li>
                            <li><a href="certificate/index.html"><img class="imgover" src="../img_common/gnavi6.png" width="233" height="48" alt="Certificate Issuance" /></a></li>
                            <li><a href="statistics/index.html"><img class="imgover" src="../img_common/gnavi7.png" width="234" height="48" alt="Statistics" /></a></li>
                            <li><a href="faq/index.html"><img class="imgover" src="../img_common/gnavi8.png" width="235" height="48" alt="FAQ" /></a></li>
                        </ul>
                    </div>
                </div><!-- /global navigation -->

                <hr />

                <div id="bodySec">
                    <div id="bodySec-inner" class="clearfix">
                        <a name="mainAnc" id="mainAnc">Start of text</a>
                        <div id="alpha">
                            <div id="h1Block">
                                <ul class="clearfix">
                                    <li><a href="http://info.jees-jlpt.jp/" target="_blank"><img class="imgover" src="../img/bt_main01.png" width="210" height="63" alt="Taking the Test in Japan" /></a></li>
                                    <li><a href="application/overseas_list.html"><img class="imgover" src="../img/bt_main02.png" width="210" height="63" alt="Taking the Test Overseas" /></a></li>
                                    <li><a href="about/levelsummary.html"><img class="imgover" src="../img/bt_main03.png" width="210" height="63" alt="Summary of Linguistic Competence Required for Each Level" /></a></li>
                                    <li><a href="guideline/results_online.html"><img class="imgover" src="../img/bt_main04.png" width="210" height="62" alt="Test Results Announcement" /></a></li>
                                </ul>
                                <h1><img src="https://www.jlpt.jp/img/index_h1Block_2020_may.jpg" width="721" height="251" alt="JLPT2020" /></h1>
                            </div> <!-- end of id="h1Block" -->

                            <hr />

                            <div class="clearfix">
                                <div id="leftBlock">
                                    <div id="ct1">
                                        <div class="h2Block clearfix" style="border: 1px #bfbfbf solid;">
                                            <h2><img src="../img/index_h2_1.png" width="41" height="13" alt="Tin túc" /></h2>
                                            <div class="link"><a href="topics/index.html"><img class="imgover" src="../img/linktext_info.png" width="93" height="11" alt="See All News" /></a></div>
                                        </div>
                                        <ul>
                                            @if (isset($list_posts))
                                            @foreach ($list_posts as $key => $data)
                                            <li>
                                                <dl class="clearfix">
                                                    <dt><span class="date">{{$data->created_at}}</span></dt>
                                                    <dd><a href="{{$data->link}}">{{$data->name}}</a></dd>
                                                </dl>
                                            </li>
                                            @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>

                                <div id="rightBlock">
                                    <div style="border: 2px red solid;border-radius: 4px;">
                                        <div><img src="../img/sche_ttl2.png" width="319" height="53" alt="Test Dates" /></div>
                                        <div id="sche">
                                            <div id="sche_in">
                                                <div><img src="https://www.jlpt.jp/e/img/sche_date01_2020.gif" width="318" height="36" alt="First test: Sunday, July 5, 2019" /></div>
                                                <div><img src="https://www.jlpt.jp/e/img/sche_date02_2020.gif" width="318" height="34" alt="Second test: Sunday, December 6, 2019" /></div>
                                                <!-- <div><img src="https://www.jlpt.jp/e/img/date_line2.gif" alt="" width="318" height="5" border="0" /></div> -->
                                            </div>

                                            <div class="sche_text"><span style="color:red;">＊ Kỳ thi vào năm 2020 (tháng 7) của JLPT đã bị hủy bỏ ở tất cả các điểm thi, ở Nhật Bản và ở nước ngoài.</span><br>
                                                ＊ Ngoài Nhật Bản, kỳ thi có thể chỉ được tổ chức vào tháng 7 hoặc tháng 12 tại một số thành phố. Nhấp chuột
                                                <a href="application/overseas_list.html"> đây </a>cho lịch trình kiểm tra ở thành phố của bạn.</div>
                                        </div>
                                    </div>
                                    <!-- end of id="sche" -->

                                    <dl id="sample">
                                        <dt><img src="../img/sample_ttl2.png" width="322" height="52" alt="Let’s Try Sample Questions!" /></dt>
                                        <dd class="m-t15"><img src="../img/sample_txt_a.png" width="222" height="17" alt="You can review sample questions" /></dd>
                                        <dd class="m-t06"><img src="../img/sample_txt_b.png" width="172" height="17" alt="of the new JLPT by level." /></dd>
                                        <dd class="btn1"><a href="https://www.jlpt.jp/e/samples/forlearners.html"><img class="imgover" src="../img/sample1018e.png" width="182" height="30" alt="Sample Questions (FLASH)" /></a></dd>
                                    </dl>

                                    <div style="margin-top:12px;"><a href="samples/sampleindex.html#anchor01"><img src="../img/sample01.png" class="imgover" alt="JLPT Official Practice Workbook Vol.2(published 2018)" width="322" height="46" /></a><br />
                                        <a href="samples/sampleindex.html#anchor02"><img style="margin-top: 3px;" src="../img/sample02.png" class="imgover" alt="JLPT Official Practice Workbook(published 2012)" width="322" height="46" /></a></div>
                                    <div id="materials"><a href="reference/materials.html"><img src="../img/materials_bt.png" class="imgover" alt="Tham khảo & tải xuống" width="322" height="82" /></a></div>

                                </div>
                            </div>
                            <!-- end of id="rightBlock" -->
                        </div>
                    </div>
                    <p id="endMainAnc">Cuối văn bản</p> <!-- /contents area -->
                </div>
            </div> <!-- /body sec -->

            <hr />

            <div id="footSec">
                <div id="footSec-inner" class="clearfix">
                    <ul class="footNavi clearfix">
                        <li><a href="policy.html">Chính sách trang web</a></li>
                        <li><a href="privacy.html">Chính sách bảo mật</a></li>
                        <li><a href="sitemap.html">Sơ đồ</a></li>
                        <li><a href="links.html">Liên kết</a></li>
                    </ul>
                    <p class="lead">Trang web chính thức về kỳ thi năng lực tiếng Nhật trên toàn thế giới được điều hành bởi Japan Foundation và các <br>Sở Giao dịch và Dịch vụ Giáo dục Nhật Bản.</p>
                    <ul class="footLogo clearfix">
                        <li><a href="http://www.jpf.go.jp/e/index.html" target="_blank"><img src="https://www.jlpt.jp/e/resource/img_common/foot_logo01.gif" width="194" height="49" alt="Japan Foundation" /></a></li>
                        <li><a href="http://www.jees.or.jp/index.htm" target="_blank"><img src="https://www.jlpt.jp/e/resource/img_common/foot_logo02.gif" width="173" height="52" alt="Japan Educational Exchanges and Services" /></a></li>
                    </ul>
                    <div id="copyright">
                        <p>Copyright (C) 2012 Quỹ Nhật Bản / Sở giáo dục và dịch vụ Nhật Bản</p>
                    </div>
                </div>
            </div> <!-- /foot sec -->
        </div>
    </div>

    </div>
    <!-- /whole -->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-16581383-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
</body>

</html>