<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Posts;
use phpDocumentor\Reflection\Types\Null_;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_posts = DB::table('posts')
            ->select('*')
            ->whereNull('deleted_at')
            ->get();
        $viewData = [
            'list_posts' => $list_posts,
        ];
        return view('posts.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->except('_token');

        $data = new Posts();
        $data['name'] = $request->name;
        $data['title'] = $request->title;
        $data['content'] = $request->content;
        $data['link'] = $request->link;
        $data->save();
        return redirect()->route('posts.index')->with('success', 'Thêm mới thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Posts::findOrFail($id);
        return view('posts.update', compact('id', 'posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->except('_token');

        $data = Posts::find($id);
        $data->name = $request->name;
        $data->title = $request->title;
        $data->content = $request->content;
        $data->link = $request->link;
        $data->save();
        return redirect()->Route('posts.index')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Posts::findOrFail($id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
