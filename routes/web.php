<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $list_posts = DB::table('posts')
        ->select(DB::raw('posts.id, posts.name, posts.title, posts.content, posts.link , DATE(posts.created_at) AS created_at'))
        ->whereNull('deleted_at')
        ->orderBy('posts.id', 'ASC')
        ->get();
    $viewData = [
        'list_posts' => $list_posts,
    ];

    return view('welcome.welcome', $viewData);
});

// Route::get('/', 'HomeController@welcome')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'users'], function () {

        Route::get('/', 'UsersController@index')->name('users.index');

        Route::get('/create', 'UsersController@create')->name('users.create');

        Route::post('/store', 'UsersController@store')->name('users.store');

        Route::get('/edit/{id}', 'UsersController@edit')->name('users.edit');

        Route::post('/update/{id}', 'UsersController@update')->name('users.update');

        Route::get('delete/{id}', 'UsersController@destroy')->name('users.delete');
    });

    Route::group(['prefix' => 'posts'], function () {

        Route::get('/', 'PostsController@index')->name('posts.index');

        Route::get('/create', 'PostsController@create')->name('posts.create');

        Route::post('/store', 'PostsController@store')->name('posts.store');

        Route::get('/edit/{id}', 'PostsController@edit')->name('posts.edit');

        Route::post('/update/{id}', 'PostsController@update')->name('posts.update');

        Route::get('delete/{id}', 'PostsController@destroy')->name('posts.delete');
    });
});
