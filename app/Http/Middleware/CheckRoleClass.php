<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleClass
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $levevlid=$request->route('levelid');
       
        switch ($levevlid) {
            case "6":
                if(auth()->user()->role == 11 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "7":
                if(auth()->user()->role == 12 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "8":
                if(auth()->user()->role == 13 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "9":
                if(auth()->user()->role == 14 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            case "10":
                if(auth()->user()->role == 15 || auth()->user()->role == 16){
                    return $next($request);
                }
                return redirect()->route('403');
                break;
            default:
                return redirect()->route('403');
            }
    }
}
