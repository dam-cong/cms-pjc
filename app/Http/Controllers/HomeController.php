<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('content');
    }

    public function welcome()
    {
        $list_posts = DB::table('posts')
            ->select('*')
            ->whereNull('deleted_at')
            ->orderBy('posts.id', 'ASC')
            ->get();
        $viewData = [
            'list_posts' => $list_posts,
        ];

        return view('welcome.welcome', $viewData);

        // return view('welcome.welcome');
    }
}
