@extends('layouts.index')
@section('content')
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Bài viết</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="{{route('home')}}">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="{{route('posts.index')}}">Danh sách</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Sửa</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="{{Route('posts.update', $id)}}" method="POST" enctype="multipart/form-data" id="formPost">
                        @csrf
                        <div class="card-header">
                            <div class="card-title">Sửa bài viết</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group form-inline">
                                        <label for="name" class="col-md-3 col-form-label">Tên bài viết</label>
                                        <div class="col-md-9 p-0">
                                            <input type="text" class="form-control input-full" name="name" id="name" placeholder="Nhập tên bài viết" value="{{$posts->name}}">
                                        </div>
                                    </div>

                                    <div class="form-group form-inline">
                                        <label for="title" class="col-md-3 col-form-label">Tiêu đề</label>
                                        <div class="col-md-9 p-0">
                                            <input type="text" class="form-control input-full" name="title" id="title" placeholder="Nhập tiêu đề" value="{{$posts->title}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group form-inline">
                                        <label for="content" class="col-md-3 col-form-label">Nội dung</label>
                                        <div class="col-md-9 p-0">
                                            <input type="text" class="form-control input-full" name="content" id="content" placeholder="Nhập nội dung" value="{{$posts->content}}">
                                        </div>
                                    </div>
                                    <div class="form-group form-inline">
                                        <label for="content" class="col-md-3 col-form-label">Đường dẫn</label>
                                        <div class="col-md-9 p-0">
                                            <input type="text" class="form-control input-full" name="link" id="link" placeholder="Nhập đường dẫn" value="{{$posts->link}}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                </div>
                <div class="card-action">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                    <a type="button" class="btn btn-danger" href="{{route('posts.index')}}">Hủy</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection