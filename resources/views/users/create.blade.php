@extends('layouts.index')
@section('content')

<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Người dùng</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="{{route('home')}}">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="{{route('users.index')}}">Danh sách</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Thêm mới</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="{{Route('users.store')}}" method="POST" enctype="multipart/form-data" id="formUsers" style="display:inline; margin:0px; padding:0px;">
                        @csrf
                        <div class="card-header">
                            <div class="card-title">Thêm mới người dùng</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group form-inline">
                                        <label for="name" class="col-md-3 col-form-label">Họ và tên</label>
                                        <div class="col-md-9 p-0">
                                            <input type="text" class="form-control input-full" name="name" id="name" placeholder="Nhập họ và tên">
                                        </div>
                                    </div>

                                    <div class="form-group form-inline">
                                        <label for="password" class="col-md-3 col-form-label">Mật khẩu</label>
                                        <div class="col-md-9 p-0">
                                            <input type="password" class="form-control input-full" name="password" id="password" placeholder="Nhập mật khẩu">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-4">
                                    <div class="form-group form-inline">
                                        <label for="email" class="col-md-3 col-form-label">Email</label>
                                        <div class="col-md-9 p-0">
                                            <input type="email" class="form-control input-full" name="email" id="email" placeholder="_@gmail.com">
                                        </div>
                                    </div>

                                    <div class="form-group form-inline">
                                        <label for="status" class="col-md-3 col-form-label">Trạng thái</label>
                                        <div class="col-md-9 p-0">
                                            <select class="form-control input-full" id="status">
                                                <option value="1">Người dùng</option>
                                                <option value="2">Giáo viên</option>
                                                <option value="3">Admin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button type="submit" class="btn btn-primary">Lưu</button>
                            <a type="button" class="btn btn-danger" href="{{route('users.index')}}">Hủy</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection